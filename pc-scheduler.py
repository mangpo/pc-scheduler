import sys, json, string
import random
import math

# Ideal values. These values can be modified.
gap_exp = 10    # Any leader has at least gap_exp number of papers after finishing leading any paper until leading the next one.
part_gap = 4    # Any leader has at least part_exp number of papers after finishing leading any paper until participating in discussion.
conf_gap = 4    # Any leader has at least conf_exp number of papers after finishing leading any paper until conflicting with some papers (leaving the room because of the conflict.)
out_gap = 2     # Any leader has at least out_exp number of papers after entering the room until leading the next paper.
leaves_exp = 4  # Anyone has to leave the room at most leaves_exp times.

# Parameters for genetic algorithm. 
n_pops = 510
n_parents = 25
n_random_pops = 10
n_children = (n_pops - n_random_pops)/n_parents

min_gap_w = 100
min_part_w = 100
min_conf_w = 100
min_out_w = 100
max_leaves_w = 100
leaves_w = 1

# Data structures to store necessary information
rev2rid = {}
rev2paper = {}
rev2conflict = {}
lead2paper = {}
exclude_papers = []
all_papers = []
epc = []
csv = {}

cands = []


class Candidate:
    def __init__(self, order):
        assert(len(order) == len(all_papers))
        self.paper2order = {}
        self.order = order
        for i in range(len(all_papers)):
            self.paper2order[order[i]] = i
        self.has_eval = False
        
    # Evaluate the candidate. The smaller the better candidate.
    def eval(self):
        if self.has_eval:
            return self.value
        
        self.has_eval = True
        self.min_gap = len(all_papers)
        self.min_gap_leader = None
        self.min_gap_papers = None
        self.sum_gap = 0

        self.min_part = len(all_papers)
        self.min_part_leader = None
        self.min_part_paper = None

        self.min_conf = len(all_papers)
        self.min_conf_leader = None
        self.min_conf_paper = None

        self.min_out = len(all_papers)
        self.min_out_leader = None
        self.min_out_paper = None
        for leader in lead2paper:
            papers = lead2paper[leader]
            discuss = rev2paper[leader]
            if leader in rev2conflict:
                conflict = rev2conflict[leader]
            else:
                conflict = []
            for i in range(len(papers)):
                for j in range(i+1, len(papers)):
                    diff = abs(self.paper2order[papers[i]] - self.paper2order[papers[j]])
                    self.sum_gap += diff
                    assert(diff > 0)
                    if diff < self.min_gap:
                        self.min_gap = diff
                        self.min_gap_leader = leader
                        self.min_gap_papers = (papers[i], papers[j])
                for paper in discuss:
                    diff = self.paper2order[paper] - self.paper2order[papers[i]]
                    if diff > 0 and diff < self.min_part:
                        self.min_part = diff
                        self.min_part_leader = leader
                        self.min_part_paper = paper
                    elif diff < 0 and -diff < self.min_out:
                        self.min_out = -diff
                        self.min_out_leader = leader
                        self.min_out_paper = paper
                for paper in conflict:
                    diff = self.paper2order[paper] - self.paper2order[papers[i]]
                    if diff > 0 and diff < self.min_conf:
                        self.min_conf = diff
                        self.min_conf_leader = leader
                        self.min_conf_paper = paper
                    elif diff < 0 and -diff < self.min_out:
                        self.min_out = -diff
                        self.min_out_leader = leader
                        self.min_out_paper = paper
                    
        self.max_leaves = 0
        self.max_leaves_rev = None
        self.sum_leaves = 0
        for rev in rev2conflict:
            papers = rev2conflict[rev]
            orders = [self.paper2order[x] for x in papers]
            orders.sort()
            if len(papers) == 0:
                leaves = 0
            else:
                leaves = 1
            for i in range(len(papers)-1):
                if orders[i+1] - orders[i] > 1:
                    leaves += 1
            self.sum_leaves += leaves
            if leaves > self.max_leaves:
                self.max_leaves = leaves
                self.max_leaves_rev = rev
                self.max_leaves_order = orders

        gap = self.min_gap
        part = self.min_part
        conf = self.min_conf
        out = self.min_out
        leaves = self.max_leaves
        if gap > gap_exp:
            gap = gap_exp
        if leaves < leaves_exp:
            leaves = leaves_exp
        if part > part_gap:
            part = part_gap
        if out > out_gap:
            out = out_gap
        if conf > conf_gap:
            conf = conf_gap

        self.value =  min_gap_w*(gap_exp - gap) + min_part_w*(part_gap - part) + min_conf_w*(conf_gap - conf) + min_out_w*(out_gap - out) + max_leaves_w*(leaves - leaves_exp) + leaves_w*(self.sum_leaves - leaves_exp*len(rev2paper))

        return self.value

    # Return a pair of two candidates: one with the best candidate and a random candidate 
    # when trying to maximize gap between 2 leads.
    def maximize_gap(self):
        # Contract a list of orders of papers 'leader' leads.
        order = [self.paper2order[x] for x in lead2paper[self.min_gap_leader]]
        order.sort()
        order = [-gap_exp] + order + [len(all_papers)+gap_exp]

        # Construct a list of valid orders to be swap. The list excludes the old orders.
        swap = []
        for i in range(len(order)-1):
            swap.extend([x for x in range(order[i]+gap_exp, order[i+1]-gap_exp)])

        # Variable to store best candidate.
        best_cand = None
        min_val = 10000

        if len(swap) == 0:
            return [self, self]

        # Random swap in case there is no better candidate.
        rand_i = random.sample(swap,1)[0]
        rand_j = random.randint(0,1)
        rand_cand = None

        for i in swap:
            for j in range(2):
                paper = self.min_gap_papers[j]
                new_order = self.order[:]
                x = self.paper2order[paper]
                y = i
                new_order[x] = self.order[y]
                new_order[y] = self.order[x]
                new_cand = Candidate(new_order)
                new_val = new_cand.eval()
                if new_val < min_val:
                    min_val = new_val
                    best_cand = new_cand
                if i == rand_i and j == rand_j:
                    rand_cand = new_cand
        return [best_cand, rand_cand]

    # Return a pair of two candidates: one with the best candidate and a random candidate 
    # when trying to maximize gaps between lead to the next participation or conflict.
    def maximize_val(self, exp, leader, paper):
        # Contract a list of orders of papers 'leader' leads.
        order = [self.paper2order[x] for x in lead2paper[leader]]
        order.sort()
        order = [-exp] + order + [len(all_papers)+out_gap-1]

        # Construct a list of valid orders to be swap. The list excludes the old orders.
        swap = []
        for i in range(len(order)-1):
            swap.extend([x for x in range(order[i]+exp, order[i+1]-out_gap+1)])

        # Variable to store best candidate.
        best_cand = None
        min_val = 10000

        if len(swap) == 0:
            return [self, self]

        # Random swap in case there is no better candidate.
        rand_i = random.sample(swap,1)[0]
        rand_cand = None

        for i in swap:
            new_order = self.order[:]
            x = self.paper2order[paper]
            y = i
            new_order[x] = self.order[y]
            new_order[y] = self.order[x]
            new_cand = Candidate(new_order)
            new_val = new_cand.eval()
            if new_val < min_val:
                min_val = new_val
                best_cand = new_cand
            if i == rand_i:
                rand_cand = new_cand
                        
        return [best_cand, rand_cand]

    def maximize_part(self):
        return self.maximize_val(part_gap, self.min_part_leader, self.min_part_paper)

    def maximize_conf(self):
        return self.maximize_val(conf_gap, self.min_conf_leader, self.min_conf_paper)

    # Return a candidate that has less number of leaves.
    def minimize_leaves(self):
        # lr indicates if the index'th leave will merged to the previous one or the next one.
        # lr = 0 is merged to the previous one.
        # lr = 1 is merged to the next one.
        lr = random.randint(0,1)
        index = random.randint(0,len(self.max_leaves_order)-1)
        if index == 0 and lr == 0:
            # Can't merge the 0th leave with the previous one.
            index = 1
        if index == len(self.max_leaves_order)-1 and lr == 1:
            # Can't merge the last leave with the next one.
            index = len(self.max_leaves_order)-2
            
        # Find the first conflicting paper of that leave.
        lmost = index
        while lmost > 0 and self.max_leaves_order[lmost] - self.max_leaves_order[lmost-1] == 1:
            lmost -= 1
            
        # Find the last conflicting paper of that leave (plus 1).
        rmost = index+1
        while rmost < len(self.max_leaves_order) and self.max_leaves_order[rmost] - self.max_leaves_order[rmost-1] == 1:
            rmost += 1
        left = self.max_leaves_order[lmost]
        right = self.max_leaves_order[rmost-1] + 1
        
        if lr == 0:
            if lmost == 0:
                return self
            # Merge with the previous one.
            insert_here = self.max_leaves_order[lmost-1] + 1
            new_order = self.order[:insert_here] + self.order[left:right] + self.order[insert_here:left] + self.order[right:]
        else:
            if rmost == len(self.max_leaves_order):
                return self
            # Merge with the next one.
            insert_here = self.max_leaves_order[rmost]
            new_order = self.order[:left] + self.order[right:insert_here] + self.order[left:right] + self.order[insert_here:]
        
        new_cand = Candidate(new_order)
        return new_cand       

    def to_string(self):
        print "max_leaves =", self.max_leaves
        print "max_leaves_rev =", self.max_leaves_rev
        print "sum_leaves =", self.sum_leaves
        print "min_gap =", self.min_gap
        print "min_gap_leader =", self.min_gap_leader
        print "min_part =", self.min_part
        print "min_part_leader =", self.min_part_leader
        print "min_conf =", self.min_conf
        print "min_conf_leader =", self.min_conf_leader
        print "min_out =", self.min_out
        print "min_out_leader =", self.min_out_leader
        print "VALUE = ", self.value

    def str(self):
        return "%d\nmax_leaves = %d\nmax_leaves_rev = %s\nsum_leaves = %d\nmin_gap = %d\nmin_gap_leader = %s\nmin_part = %d\nmin_part_leader = %s\nmin_conf = %d\nmin_conf_leader = %s\nmin_out = %d\nmin_out_leader = %s\n" % (self.value, self.max_leaves, self.max_leaves_rev, self.sum_leaves, self.min_gap,  self.min_gap_leader, self.min_part, self.min_part_leader, self.min_conf, self.min_conf_leader, self.min_out, self.min_out_leader)

# Output the schedule as csv file.
def gen_csv(order, argv):
    if '-o' in argv:
        index = argv.index('-o') + 1
        name = argv[index]
    else:
        name = 'order.cvs'

    w = open(name,'w')
    for paper in order:
        w.write(csv[paper])

# Generate a row in csv format for a given paper.
# This procedure can be modified according to the desired format.
def gen_csv_row(paper):
    out = ""
    out += paper["number"] + ";"
    out += paper["title"] + ";"
    out += paper["authors"] + ";"

    conflict = paper["conflicts"]
    for rev in epc:
        index = conflict.find(rev)
        if index > 0:
            #print "conflict", rev
            conflict = conflict[:index] + conflict[index+len(rev)+2:]
            #print "result", conflict
    out += conflict + ";"
    out += paper["lead"] + ";"
    reviews = paper["reviews"]
    for review in reviews:
        out += review[0] + ";"
        out += review[1] + ";"
        out += review[2] + ";"
    out = filter(lambda x: x in string.printable, out)
    return out + "\n"

# Parse files into data structures storing necessary information for 
# genetic algorithm step.
def parse(argv):
    # -d option
    # Disgard some papers from the list.
    if '-d' in argv:
        index = argv.index('-d') + 1
        f = open(argv[index], 'r')
        line = f.readline()
        while line:
            exclude_papers.append(int(line))
            line = f.readline()

    # -e option.
    # Filter out external program committee from the pc meeting.
    if '-e' in argv:
        index = argv.index('-e') + 1
        f = open(argv[index], 'r')
        line = f.readline()
        while line:
            tokens = line.strip('\n').split(' ')
            name = ""
            for token in tokens:
                if token != '' and token != '\n':
                    name = name + ' ' + token
            name = name.strip(' ')
            epc.append(name)
            line = f.readline()

    # Read from the main json file.
    f = open(argv[1], 'r')
    read_data = f.read()
    papers = json.loads(read_data)
    for paper in papers:
        paper_id = int(paper["number"])
        
        # Create a map of paper id to information of the paper in csv format.
        csv[paper_id] = gen_csv_row(paper)

        # Filter out papers in the disgarded list.
        if paper_id in exclude_papers:
            continue

        # Add paper to the pool.
        all_papers.append(paper_id)

        # Create a map from a reviewer to a set of paper he/she reviews.
        reviews = paper["reviews"]
        for review in reviews:
            if review in epc:
                continue
            name = review[0]
            score = review[1]
            expert = review[2]
            if name in rev2paper:
                rev2paper[name].append(paper_id) # or paper_name
            else:
                rev2paper[name] = [paper_id]

        # Create a map from a reviewer to a set of paper he/she has conflict with.
        conflicts = paper["conflicts"]
        for name in conflicts.split(", "):
            if name == '' or name in epc:
                continue
            if name in rev2conflict:
                rev2conflict[name].append(paper_id) # or paper_name
            else:
                rev2conflict[name] = [paper_id]

        # Update a map from leaders to a list of paper he/she leads.
        leader = paper["lead"]
        if leader:
            #print leader
            if leader in lead2paper:
                lead2paper[leader].append(paper_id)
            else:
                lead2paper[leader] = [paper_id]

    # Create IDs for the reviewers.
    i = 0
    for rev in rev2paper:
        rev2rid[rev] = i
        i += 1

    for rev in rev2conflict:
        if not rev in rev2rid:
            rev2rid[rev] = i
            i += 1

    # option -l
    # Create a map from a leader to a set of paper he/she leads.
    # This option is used when "leader" field is not in the main json file.
    if '-l' in argv:
        lead2paper.clear()
        index = argv.index('-l') + 1
        f = open(argv[index], 'r')
        read_data = f.read()
        leaders = json.loads(read_data)
        for leader in leaders:
            papers = leaders[leader]
            l = []
            for p in papers:
                # if not (paper in exclude_papers):
                #     l.append(paper)
                paper = int(p)
                if paper in all_papers:
                    l.append(paper)
            lead2paper[leader] = l
            if not leader in rev2conflict:
                rev2conflict[leader] = []


# Generate n number of children from a given parent.
# num_swap is the number of the papers that have to be swap.
def mutate(parent, num_swap, n):
    count = 1
    mutants = [parent]

    # Heuristic to minimize number of leaves
    if parent.max_leaves > leaves_exp:
        mutants.append(parent.minimize_leaves()) # 1
        mutants.append(parent.minimize_leaves()) # 1
        count += 2

    # Heuristic to maximize number of papers between leads
    if parent.min_gap < gap_exp:
        mutants.extend(parent.maximize_gap()) # 2
        count += 2

    # Heuristic to maximize number of papers after leading until participating.
    if parent.min_part < part_gap:
        mutants.extend(parent.maximize_part()) # 2
        count += 2

    # Heuristic to maximize number of papers after leading until conflicting.
    if parent.min_conf < conf_gap:
        mutants.extend(parent.maximize_conf()) # 2
        count += 2

    # Randomly mutate parent by randomly shuffling num_swap random papers.
    for i in range(n-count):
        selected = random.sample(range(len(all_papers)), num_swap)
        mutant_order = parent.order[:]
        if num_swap > 2:
            permute = selected[:]
            random.shuffle(permute)
            while permute == selected:
                random.shuffle(permute)
            for j in range(num_swap):
                mutant_order[selected[j]] = parent.order[permute[j]]
        else:
            mutant_order[selected[0]] = parent.order[selected[1]]
            mutant_order[selected[1]] = parent.order[selected[0]]

        mutants.append(Candidate(mutant_order))
            
    return mutants

# Genetic algorithm with simulated annealing.
def ga():
    new_cands = []
    for i in range(n_pops):
        order = all_papers[:]
        random.shuffle(order)
        new_cands.append(Candidate(order))

    cands = sorted(new_cands, key=lambda cand:cand.eval())
    t = 0
    best_val = 10000
    best_sol = None
    stagnant = 0

    # Loop 1000 times or until we don't find better candidate in 20 steps.
    while t < 1000 and stagnant < 20:
        changes = int(10*math.e**(-1.0/30*t))
        if changes > len(all_papers)/2:
            changes = len(all_papers)/2
        if changes < 2:
            changes = 2
        new_cands = []

        # Generate new candidates by mutating parents.
        for cand in cands[:n_parents]:
            new_cands.extend(mutate(cand, changes, n_children))

        # Generate totally random candidates.
        for i in range(n_random_pops):
            order = all_papers[:]
            random.shuffle(order)
            new_cands.append(Candidate(order))

        cands = sorted(new_cands, key=lambda cand:cand.eval())
        print ">>>>> t =", t
        print cands[0].to_string()
        if cands[0].eval() < best_val:
            best_val = cands[0].eval()
            stagnant = 0
        else:
            stagnant += 1
        t += 1

    return cands[0].order


if __name__ == "__main__":
    if '-h' in sys.argv or '--help' in sys.argv:
        print "usage: python pc-scheduler.py file [-o output_file] [-d discard_file] [-e epc_file] [-l lead_file]\n"
        print "-d discard_file\t: discard the paper in discard_file from scheduling. \n\t\t  discard_file contains paper IDs separated by line break."
        print "-e epc_file\t: exclude external committee members from scheduling. \n\t\t  epc_file contains names of external committee members \n\t\t  separate by line break. \n\t\t  Names have to match with the names in the main json file \n\t\t  (e.g. lower/upper case letter, and first/last name order), \n\t\t  but number of white spaces can be arbitrary."
        print "-l lead_file\t: use this option to overwrite \"lead\" field in the main json file. \n\t\t  lead_file is a json file that map names of the leaders \n\t\t  to lists of paper IDs they lead. \n\t\t  Names have to exactly match with the names in the main json file \n\t\t  (e.g. lower/upper case letter, first/last name order, \n\t\t  and white spaces)."
        print "-o output_file\t: name the output cvs file. The default name is \'order\'.\n"
        print "Try running \"python pc-scheduler.py example/papers.json -d example/discard.txt -e example/epc.txt -l example/lead.json\" as an working example. Look at those files for the correct formats."
    else:
        parse(sys.argv)
        sol = ga()
        gen_csv(sol, sys.argv)
